<?php

/**
 * Field handler to enable embedding a form in a view.
 */
class vef_handler_field_form extends views_handler_field {
  function query() {
    // do nothing -- to override the parent query.
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['form_id'] = array('default' => '');
    $options['arguments'] = array('default' => '');
    $options['tokenize'] = array('default' => FALSE, 'bool' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    _views_embed_form_configuration_options($form, $this);
    // Since we disallow advanced_render, no need to show this empty fieldset.
    $form['alter']['#access'] = FALSE;
  }

  function allow_advanced_render() {
    return FALSE;
  }

  function render($values) {
    if (!array_key_exists($this->options['form_id'], _views_embed_form_get_forms())) {
      watchdog('views_embed_form', 'Illegal access to form %form', array('%form' => $this->options['form_id']), WATCHDOG_WARNING);
      return '';
    }

    $arg_string = trim($this->options['arguments']);
    if (strlen($arg_string) > 0) {
      if ($this->options['tokenize']) {
        $tokens = $this->get_render_tokens(NULL);
        $arg_string = filter_xss_admin($arg_string);
        $arg_string = strtr($arg_string, $tokens);
      }
      $args = preg_split('!(\r|\n|\r\n)!', $arg_string);
      for ($i = 0; $i < count($args); $i++) {
        if ($args[$i] == 'NULL') {
          $args[$i] = NULL;
        }
      }
    }

    array_unshift($args, $this->options['form_id']);
    $form = call_user_func_array('drupal_get_form', $args);
    return drupal_render($form);
  }
}
