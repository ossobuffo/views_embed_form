<?php

/**
 * Views area form handler.
 *
 * @ingroup views_area_handlers
 */
class vef_handler_area_form extends views_handler_area {

  function option_definition() {
    $options = parent::option_definition();
    $options['form_id'] = array('default' => '');
    $options['arguments'] = array('default' => '');
    $options['tokenize'] = array('default' => FALSE, 'bool' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    _views_embed_form_configuration_options($form, $this);
  }

  function render($empty = FALSE) {
    if (!array_key_exists($this->options['form_id'], _views_embed_form_get_forms())) {
      watchdog('views_embed_form', 'Illegal access to form %form', array('%form' => $this->options['form_id']), WATCHDOG_WARNING);
      return '';
    }

    if (!$empty || !empty($this->options['empty'])) {
      $arg_string = trim($this->options['arguments']);
      if (strlen($arg_string) > 0) {
        $args = preg_split('!(\r|\n|\r\n)!', $arg_string);
        for ($i = 0; $i < count($args); $i++) {
          if ($args[$i] == 'NULL') {
            $args[$i] = NULL;
          }
          elseif ($this->options['tokenize']) {
            $args[$i] = $this->view->style_plugin->tokenize_value($args[$i], 0);
          }
        }
      }
      else {
        $args = array();
      }
      array_unshift($args, $this->options['form_id']);
      $form = call_user_func_array('drupal_get_form', $args);
      return drupal_render($form);
    }
    return '';
  }
}
