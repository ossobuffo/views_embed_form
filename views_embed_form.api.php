<?php
/**
 * @file
 * Describe hooks provided by the Views Embed Form module.
 */

/**
 * Enumerates a whitelist of forms which may be embedded in views.
 *
 * The array keys should be valid Drupal form IDs, and the corresponding
 * values should be localized human-readable descriptions which will appear
 * in the Views UI.
 *
 * @return array
 */
function hook_views_embed_form() {
  return array(
    'search_form' => t('Search the site for a keyword')
  );
}